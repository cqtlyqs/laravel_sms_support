
#### 介绍
sms component for laravel

#### 使用说明
step1.  安装 
```
composer require ktnw/sms 
```
step2.  发布
```angular2html
php artisan vendor:publish --provider="Ktnw\sms\Providers\SmsServiceProvider"
```

step3.  执行命令
```angular2html
php artisan support:sms
```
step4.  配置config/smsConfig.php <br>
step5.  订单日志表数据迁移
database/migrations/2022_01_21_135810_create_sms_log.php可根据业务需求自行扩展。<br>
扩展后执行迁移: <br>
```
php artisan migrate --path database/migrations/2022_01_21_135810_create_sms_log.php
```
若未进行扩展，也可执行以下命令进行迁移: 
```
php artisan migrate --path vendor/ktnw/sms/src/database/migrations/2022_01_21_135810_create_sms_log.php
```

注意: 1. step2中会把SmsController发布到app\Http\Controllers\Api目录中。若不需要，可自行删除。<br>
     2. SmsController中包含基本的发送短信相关的功能。 <br>
         1). 获取图形验证码; 2). 校验图形验证码; 3). 发送短信验证码; 4). 校验短信验证码 <br>
     3. 可根据业务需求，自行扩展SmsController的功能。 <br>
     4. 发送短信功能可参见SmsServiceProvider中的send方法。 <br>
     5. 可根据业务需求，继承SmsBaseService类，自行扩展其他功能。<br>
     

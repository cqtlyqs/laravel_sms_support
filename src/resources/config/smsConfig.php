<?php
// 发送短信验证码的配置文件
return [
    'sms_accessKey_id'     => '',          // 发送短信的AccessKey Id
    'sms_accessKey_secret' => '',          // 发送短信的AccessKeySecret
    'sms_regionId'         => '',          // 区域
    'sms_template'         => '',          // 短信验证码模板
    'sms_sign_name'        => '',          // 短信签名
    'sms_effective_time'   => 10,          // 短信验证码有效期, 单位:分钟
    'check_image_code'     => false,       // 发送短信时，是否校验图形验证码
    'last_sms_cache_key'   => "last_",     // 缓存发送短信日志的key
    'send_interval_time'   => 45,          // 发送短信间隔时间
    'sms_log_table_name'   => "sms_log",   // 保存短信日志的表名
    'save_sms_log'         => true,        // 是否保持发送短信日志
];
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Ktnw\sms\utils\Config;

/**
 * 短信发送日志表
 * 表名及表字段可根据实际业务修改
 *
 */
class CreateSmsLog extends Migration
{

    /**
     * @var mixed
     */
    private $tableName;

    /**
     *
     */
    public function __construct()
    {
        $this->tableName = Config::getConfigValue("smsConfig.sms_log_table_name");
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create($this->tableName, function (Blueprint $table) {
            $table->charset   = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';
            $table->engine    = 'InnoDB';
            $table->bigIncrements('id')->nullable(false)->comment('主键,字段不可更改');
            $table->string('phone', 20)->nullable(false)->comment('手机号,字段不可更改');
            $table->string('send_status', 1)->nullable(false)->comment('发送状态Y:发送成功.N:发送失败. 字段不可更改');
            $table->string('sms_template', 50)->nullable(false)->comment('短信模板, 字段不可更改');
            $table->text('content')->nullable(false)->comment('短信验证码或发送的短信内容');
            $table->string('send_from', 50)->nullable()->comment('发送来源');
            $table->dateTime("created_at")->comment("创建时间, 字段不可更改");
            $table->dateTime("valid_end_time")->nullable(false)->comment("有效期截止时间, 字段不可更改");
            $table->tinyInteger("use_status")->default(1)->comment("是否使用, 1-未使用;2-已使用; 字段不可更改");
            $table->dateTime("updated_at")->comment("更新时间")->nullable(true);
            $table->index(['phone', 'sms_template'], "idx_phone_smstemplate");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->tableName);
    }
}

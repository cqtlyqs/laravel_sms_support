<?php

namespace Ktnw\sms\utils;
/**
 * 图形验证码相关功能
 */
class ImageCodeUtils
{
    /**
     * 获取图形验证码
     * @param string $config
     * @return string
     */
    public static function captchaImg(string $config = 'default'): string
    {
        return captcha_img($config);
    }

    /**
     * 获取图形验证码
     * @param string $config
     * @return string
     */
    public static function captchaSrc(string $config = 'default'): string
    {
        return captcha_src($config);
    }

    /**
     * 校验图形验证码
     * @param string $imgCode
     * @param string $imgKey
     * @param string $config
     * @return bool
     */
    public static function checkImgCode(string $imgCode, string $imgKey, string $config = 'default'): bool
    {
        return !(empty($imgCode) || empty($imgKey)) && captcha_api_check($imgCode, $imgKey, $config);
    }

}
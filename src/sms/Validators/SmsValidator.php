<?php

namespace Ktnw\sms\Validators;


use Prettus\Validator\LaravelValidator;

/**
 * Class IndexValidator.
 *
 * @package namespace App\Validators;
 */
class SmsValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        "send" => [
            'phone' => ['required', 'regex:/^1[1-9][0-9]{9}$/'],
        ]
    ];

}

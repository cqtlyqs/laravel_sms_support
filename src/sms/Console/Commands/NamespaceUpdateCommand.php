<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;

class NamespaceUpdateCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'namespace:update {className} {namespace}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'update class namespace.';


    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'update namespace';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $className = $this->argument("className");
        $namespace = $this->argument("namespace");
        $path      = app_path($className);
        if (!file_exists($path)) {
            $this->info("file[$path] not exist.");
            $this->info($this->type . ' created fail.');
            return;
        }
        $content = File::get($path);
        File::put($path, preg_replace("/namespace.*/", "namespace " . $namespace . ";", $content));
        $this->info($this->type . ' created successfully.');
    }

}

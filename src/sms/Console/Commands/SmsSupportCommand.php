<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;

class SmsSupportCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'support:sms';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'update class namespace.';

    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'support sms';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        // step1: 更新namespace
        $this->namespaceUpdate();
        $this->info($this->type . ' created successfully.');
    }

    private function namespaceUpdate()
    {
        $classes = [
            ["classPath" => 'Http\Controllers\Api\SmsController.php', 'namespace' => 'App\Http\Controllers\Api'],
        ];
        foreach ($classes as $item) {
            $path = app_path($item['classPath']);
            if (!file_exists($path)) {
                $this->info("namespace update fail: file[$path] not exist.");
                return;
            }
            $content = File::get($path);
            File::put($path, preg_replace("/namespace.*/", "namespace " . $item['namespace'] . ";", $content));
        }
    }


}
